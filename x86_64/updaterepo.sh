#!/bin/bash

#!/bin/bash

rm redfoxos-repo*

echo "repo-add"
repo-add -s -n -R redfoxos-repo.db.tar.gz *.pkg.tar.zst

sleep 1

rm redfoxos-repo.db
rm redfoxos-repo.db.sig

rm redfoxos-repo.files
rm redfoxos-repo.files.sig

mv redfoxos-repo.db.tar.gz redfoxos-repo.db
mv redfoxos-repo.db.tar.gz.sig redfoxos-repo.db.sig

mv redfoxos-repo.files.tar.gz redfoxos-repo.files
mv redfoxos-repo.files.tar.gz.sig redfoxos-repo.files.sig
echo "####################################"
echo "Repo Updated!!"
echo "####################################"